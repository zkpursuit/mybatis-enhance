package org.mybatis.enhance.annotation;

import java.lang.annotation.*;

/**
 * 对数据库映射实体JavaBean中某个字段标记为主键
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PrimaryKey {
    /**
     * 是否为自增主键
     *
     * @return true为自增主键
     */
    boolean autoIncrement() default false;
}

