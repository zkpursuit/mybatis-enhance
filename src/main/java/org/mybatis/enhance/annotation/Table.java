package org.mybatis.enhance.annotation;

import java.lang.annotation.*;

/**
 * 数据库表映射实体JavaBean
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Table {
    /**
     * 数据库表名
     *
     * @return 数据库表名
     */
    String value();
}

