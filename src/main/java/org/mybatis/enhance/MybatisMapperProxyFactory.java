package org.mybatis.enhance;

import org.apache.ibatis.binding.MapperProxyFactory;
import org.apache.ibatis.session.SqlSession;

import java.lang.reflect.Proxy;

/**
 * @author zkpursuit
 */
public class MybatisMapperProxyFactory<T> extends MapperProxyFactory<T> {

    public MybatisMapperProxyFactory(Class<T> mapperInterface) {
        super(mapperInterface);
    }

    @SuppressWarnings("unchecked")
    protected T newInstance(MybatisMapperProxy<T> mapperProxy) {
        return (T) Proxy.newProxyInstance(this.getMapperInterface().getClassLoader(), new Class[]{this.getMapperInterface()}, mapperProxy);
    }

    public T newInstance(SqlSession sqlSession) {
        MybatisMapperProxy<T> mapperProxy = new MybatisMapperProxy(sqlSession, this.getMapperInterface(), this.getMethodCache());
        return this.newInstance(mapperProxy);
    }
}

