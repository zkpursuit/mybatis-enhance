package org.mybatis.enhance;

import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * mybatis通用Mapper
 *
 * @param <T> 数据库表实体类型
 * @author zkpursuit
 */
public interface Mapper<T> {

    T selectById(Serializable id);

    List<T> selectByIds(@Param("list") Set<? extends Serializable> ids);

    List<T> selectAll();

    /**
     * 查询数量
     *
     * @param condition 查询条件，必须赋值 selectFields，比如 count(*)
     * @return 数量
     */
    Integer selectCountByCondition(@Param("condition") Condition condition);

    List<T> selectListByCondition(@Param("condition") Condition condition);

    T selectOneByCondition(@Param("condition") Condition condition);

    int insertOne(T entity);

    int batchInsert(@Param("list") List<T> list);

    int updateById(T entity);

    int updateFieldsById(@Param("params") Map<String, Object> params);

    int updateNotNullFieldsById(T entity);

    int deleteById(Serializable id);

    int deleteByIds(@Param("list") Set<? extends Serializable> ids);

    int deleteByCondition(@Param("condition") Condition condition);

    int updateByCondition(@Param("entity") T entity, @Param("condition") Condition condition);

}

