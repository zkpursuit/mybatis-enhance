package org.mybatis.enhance;

import com.kaka.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * mybatis条件构造器
 *
 * @author zkpursuit
 */
public class Condition {

    public static class Order {
        private String type;
        private String field;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }
    }

    public static class Item {
        private int id;
        private String field;
        private Object value;

        public Item(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }

    private List<String> selectFields;
    private Order order;
    private final List<Item> items = new ArrayList<>();

    public static Condition create() {
        return new Condition();
    }

    public Condition selectFields(String... field) {
        if (field.length > 0) {
            if (selectFields == null) {
                selectFields = new ArrayList<>(field.length);
            } else {
                selectFields.clear();
            }
            selectFields.addAll(Arrays.asList(field));
        }
        return this;
    }

    /**
     * 排序
     *
     * @param orderType
     * @param field
     * @return
     */
    public Condition orderBy(String orderType, String field) {
        if (order == null) {
            order = new Order();
        }
        order.setType(orderType);
        order.setField(field);
        return this;
    }

    /**
     * 左括号
     *
     * @return
     */
    public Condition parenthesesOpen() {
        items.add(new Item(1000));
        return this;
    }

    /**
     * 右括号
     *
     * @return
     */
    public Condition parenthesesClose() {
        items.add(new Item(2000));
        return this;
    }

    /**
     * 小于
     *
     * @param field
     * @param value
     * @return
     */
    public Condition lessThan(String field, Object value) {
        Item item = new Item(2);
        item.setField(field);
        item.setValue(value);
        items.add(item);
        return this;
    }

    /**
     * 大于
     *
     * @param field
     * @param value
     * @return
     */
    public Condition greaterThan(String field, Object value) {
        Item item = new Item(3);
        item.setField(field);
        item.setValue(value);
        items.add(item);
        return this;
    }

    /**
     * 等于
     *
     * @param field
     * @param value
     * @return
     */
    public Condition equal(String field, Object value) {
        Item item = new Item(4);
        item.setField(field);
        item.setValue(value);
        items.add(item);
        return this;
    }

    /**
     * 不等于
     *
     * @param field
     * @param value
     * @return
     */
    public Condition notEqual(String field, Object value) {
        Item item = new Item(5);
        item.setField(field);
        item.setValue(value);
        items.add(item);
        return this;
    }

    /**
     * 与
     *
     * @return
     */
    public Condition and() {
        items.add(new Item(100));
        return this;
    }

    /**
     * 或
     *
     * @return
     */
    public Condition or() {
        items.add(new Item(200));
        return this;
    }

    public Condition between(String field, Object min, Object max) {
        return this.lessThan(field, max).and().greaterThan(field, min);
    }

    public Condition in(String field, Collection<?> value) {
        Item item = new Item(6);
        item.setField(field);
        String valueStr = StringUtils.group(value, "(", ",", ")");
        item.setValue(valueStr);
        items.add(item);
        return this;
    }

    public List<String> getSelectFields() {
        return selectFields;
    }

    public Order getOrder() {
        return order;
    }

    public List<Item> getItems() {
        return items;
    }
}

