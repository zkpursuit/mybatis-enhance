package org.mybatis.enhance;

import com.kaka.util.ReflectUtils;
import org.apache.ibatis.session.Configuration;

/**
 * @author zkpursuit
 */
public class MybatisConfiguration extends Configuration {

    public MybatisConfiguration() {
        super();
        MybatisMapperRegistry mybatisMapperRegistry = new MybatisMapperRegistry(this);
        ReflectUtils.setFieldValue(this, "mapperRegistry", mybatisMapperRegistry);
    }

}
