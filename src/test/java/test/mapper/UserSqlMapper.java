package test.mapper;

import org.mybatis.wrap.Mapper;
import test.entity.User;

public interface UserSqlMapper extends Mapper<User> {
}
