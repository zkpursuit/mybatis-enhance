package test.entity;

import org.mybatis.wrap.annotation.PrimaryKey;
import org.mybatis.wrap.annotation.Table;

import java.io.Serializable;

@Table("t_user")
public class User implements Serializable {
    @PrimaryKey
    private Long uid;
    private String name;
    private int sex;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }
}
