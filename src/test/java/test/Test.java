package test;

import com.kaka.util.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.wrap.Condition;
import test.entity.User;
import test.mapper.UserSqlMapper;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        try (SqlSession session = Mybatis.getInstance().getSqlSessionFactory().openSession()) {
            UserSqlMapper userSqlMapper = session.getMapper(UserSqlMapper.class);
            User user = userSqlMapper.selectById(1L);
            System.out.println(user);

            List<User> userList = userSqlMapper.selectListByCondition(Condition.create().between("uid", 1L, 100L));
            System.out.println(userList.size());

            int count = userSqlMapper.selectCountByCondition(Condition.create().selectFields("count(*)"));
            System.out.println(count);

            User user1 = new User();
            user1.setUid(50L);
            user1.setName("dsafdsf");
            user1.setSex(1);
            int addedCount = userSqlMapper.insertOne(user1);
            System.out.println(addedCount);

            List<User> addList = new ArrayList<>();
            for(long i = 100; i < 105; i++) {
                User user2 = new User();
                user2.setUid(i);
                user2.setName(StringUtils.randomString(5, true));
                user2.setSex(1);
                addList.add(user2);
            }
            addedCount = userSqlMapper.batchInsert(addList);
            System.out.println(addedCount);

            int line = userSqlMapper.deleteById(4L);
            System.out.println(line);

            session.commit();
        }
    }

}
